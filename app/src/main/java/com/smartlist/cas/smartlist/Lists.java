package com.smartlist.cas.smartlist;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Lists extends AppCompatActivity
{
    protected ListView lista;
    protected int[] ids;
    protected String[] listas;
    protected String[] fechas;
    protected String fecha;
    protected String enviarnombre;
    protected int id;
    protected int i;
    protected EditText nombreLista;

    protected boolean cancelar;

    protected AdapterDaitems adaptador;
    protected Daitems item;

    //*****************************//
    private SQLiteDatabase baseDatos;
    private static final String TAG = "bdlistapersonal";
    private static final String nombreBD = "listapersonal";

    private static final String tablaListas = "Listas";

    private static final String sqlListas = "CREATE TABLE if not exists Listas (Id INTEGER PRIMARY KEY AUTOINCREMENT, Nombre TEXT not null unique, Fecha TEXT not null);";
    private static final String sqlProductos = "CREATE TABLE if not exists Productos (IdLista INTEGER not null, Producto TEXT not null, Cantidad INTEGER not null, Precio REAL not null);";
    //*****************************//
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);


        i = 0;
        cancelar = false;
        lista = (ListView) findViewById(R.id.listViewListas);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

        fecha = df.format(Calendar.getInstance().getTime());

        //Abrir la base de datos, se creara si no existe
        abrirBasedatos();
        llenarLista();

        ImageButton regresar = (ImageButton) findViewById(R.id.ibuttonRetrocederMM);
        regresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Lists.this, MainMenu.class);
                baseDatos.close();
                startActivity(i);
            }
        });

        ImageButton botonlistas = (ImageButton) findViewById(R.id.ibuttonAgregarLista);
        botonlistas.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                NombreLista ();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent i = new Intent(Lists.this, ListTable.class);
                id = ids[position];
                enviarnombre = listas[position];
                i.putExtra("id", id);
                i.putExtra("nombre", enviarnombre);
                baseDatos.close();
                startActivity(i);
            }
        });


        //ADS
        mInterstitialAd = new InterstitialAd(this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
               // showInterstitial();
            }
        });
    }

    @Override
    public void onBackPressed ()
    {
        Intent i = new Intent(Lists.this, MainMenu.class);
        baseDatos.close();
        startActivity(i);
    }

    public void NombreLista ()
    {
        nombreLista = new EditText(this);
        nombreLista.setGravity(Gravity.CENTER_HORIZONTAL);

        //nombreLista.requestFocus();     //nuevo

        TextView title = new TextView(this);
        title.setText("Nueva Lista");
        title.setPadding(0, 50, 0, 0);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(20);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(title)
                .setView(nombreLista)
                .setMessage("Ingrese el título de la nueva lista")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1)
                    {
                        Cursor c = baseDatos.rawQuery("SELECT * FROM " + tablaListas + ";", null);

                        try
                        {
                            if (c.moveToFirst())
                            {
                                do
                                {
                                    if (c.getString(1).equalsIgnoreCase(nombreLista.getText().toString()))
                                    {
                                        cancelar = true;
                                    }
                                }while(c.moveToNext());

                                if (!cancelar)
                                {
                                    insertarLista(nombreLista.getText().toString());
                                    llenarLista();

                                    /////////////////////////////////////////////////// nuevo
                                    Intent i = new Intent(Lists.this, ListTable.class);
                                    enviarnombre = listas[id - 1];
                                    int enviarid = ids[id - 1];
                                    i.putExtra("id", enviarid);
                                    i.putExtra("nombre", enviarnombre);
                                    baseDatos.close();
                                    startActivity(i);
                                    ///////////////////////////////////////////////////
                                }
                                else
                                {
                                    AlertDialog.Builder builder2 = new AlertDialog.Builder(Lists.this);
                                    builder2.setTitle("Error");
                                    builder2.setMessage("Ya existe una lista con ese nombre");
                                    builder2.setPositiveButton("OK",null);
                                    builder2.create();
                                    builder2.show();

                                    cancelar = false;
                                }
                            }
                            else
                            {
                                insertarLista(nombreLista.getText().toString());
                                llenarLista();

                                /////////////////////////////////////////////////// nuevo
                                Intent i = new Intent(Lists.this, ListTable.class);
                                enviarnombre = listas[id - 1];
                                int enviarid = ids[id - 1];
                                i.putExtra("id", enviarid);
                                i.putExtra("nombre", enviarnombre);
                                baseDatos.close();
                                startActivity(i);
                                ///////////////////////////////////////////////////
                            }
                        }
                        catch (Exception e)
                        {
                            Log.i(TAG, "No existe tabla" + e);
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1)
                    {
                        //Cancelar
                    }
                });

        AlertDialog dialogoNombre = builder.create();
        dialogoNombre.setView(nombreLista, 50, 0, 50, 0);
        dialogoNombre.show();

        TextView messageView = (TextView)dialogoNombre.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }

    private void abrirBasedatos()
    {
        try
        {
            baseDatos = openOrCreateDatabase(nombreBD, MODE_WORLD_WRITEABLE, null);
            baseDatos.execSQL(sqlListas);
            baseDatos.execSQL(sqlProductos);
        }
        catch (Exception e)
        {
            Log.i(TAG, "Error al abrir o crear la base de datos" + e);
        }
    }

    private void llenarLista()
    {
        Cursor c = baseDatos.rawQuery("SELECT * FROM Listas", null);
        ArrayList<Daitems> daitems = new ArrayList<Daitems>();

        try
        {
            if (c.moveToFirst())
            {
                listas = new String[c.getCount()];
                fechas = new String[c.getCount()];
                ids = new int[c.getCount()];

                do
                {
                    ids[i] = c.getInt(0);
                    listas[i] = c.getString(1);
                    fechas[i] = c.getString(2);
                    item = new Daitems(listas[i], fechas[i]);
                    daitems.add(item);
                    i++;
                }while(c.moveToNext());

                id = ids.length; ///////////////////// nuevo

                cancelar = false;
                i = 0;

                adaptador = new AdapterDaitems(Lists.this, daitems);
                lista.setAdapter(adaptador);
            }
        }
        catch (Exception e)
        {
            Log.i(TAG, "No existe tabla" + e);
        }
    }

    private boolean insertarLista(String nombre)
    {
        ContentValues values = new ContentValues();
        values.put("Nombre", nombre);
        values.put("Fecha", fecha);

        return (baseDatos.insert(tablaListas, null, values) > 0);
    }
    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}