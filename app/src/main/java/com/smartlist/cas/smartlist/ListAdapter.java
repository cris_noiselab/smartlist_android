package com.smartlist.cas.smartlist;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by Carlos on 23/07/2017.
 */

public class ListAdapter extends ArrayAdapter<String>
{
    private Activity activity;
    ArrayList<String> listas;

    public ListAdapter(Activity activity, ArrayList<String> lists)
    {
            super(activity, R.layout.da_items);
            this.activity = activity;
            this.listas = lists;
    }

    static class ViewHolder
    {
    }

    public int getContador()
    {
        return listas.size();
    }

    public long getPosicion(int position)
    {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        View view = null;
        return view;
    }
}