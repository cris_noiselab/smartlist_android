package com.smartlist.cas.smartlist;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class EditItem extends AppCompatActivity
{
    protected int id;
    protected String nombrerecibido;

    protected String nombreantiguo;
    protected int cantidadantiguo;
    protected float precioantiguo;

    protected String producto;
    protected int cantidad;
    protected float precio;
    protected boolean cancelar;

    protected EditText nombreitem;
    protected EditText cantidaditem;
    protected EditText precioitem;

    private SQLiteDatabase baseDatos;
    private static final String TAG = "bdlistapersonal";
    private static final String nombreBD = "listapersonal";

    private static final String tablaProductos = "Productos";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);


        cancelar = false;

        //Abrir la base de datos, se creara si no existe
        abrirBasedatos();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            id = Integer.parseInt(extras.get("id").toString());
            nombrerecibido = extras.get("nombre").toString();

            nombreantiguo = extras.get("nombreantiguo").toString();
            cantidadantiguo = Integer.parseInt(extras.get("cantidadantiguo").toString());
            precioantiguo = Float.parseFloat(extras.get("precioantiguo").toString());
        }

        nombreitem = (EditText) findViewById(R.id.editTextNombreItem2);
        nombreitem.setText(nombreantiguo);
        cantidaditem = (EditText) findViewById(R.id.editTextCantidadItem2);
        cantidaditem.setText(String.valueOf(cantidadantiguo));
        precioitem = (EditText) findViewById(R.id.editTextPrecioItem2);
        precioitem.setText(String.valueOf(precioantiguo));

        ImageButton regresar = (ImageButton) findViewById(R.id.ibuttonRetrocederListTable2);
        regresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(EditItem.this, ListTable.class);
                baseDatos.close();
                i.putExtra("id", id);
                i.putExtra("nombre", nombrerecibido);
                startActivity(i);
            }
        });

        Button guardar = (Button) findViewById(R.id.buttonGuardar2);
        guardar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!nombreitem.getText().toString().isEmpty() && !cantidaditem.getText().toString().isEmpty() && !precioitem.getText().toString().isEmpty())
                {
                    producto = nombreitem.getText().toString();
                    cantidad = Integer.parseInt(cantidaditem.getText().toString());
                    precio = Float.parseFloat(precioitem.getText().toString());

                    if (nombreantiguo.equalsIgnoreCase(producto))
                    {
                        actualizarProducto();

                        Intent i = new Intent(EditItem.this, ListTable.class);
                        i.putExtra("id", id);
                        i.putExtra("nombre", nombrerecibido);
                        startActivity(i);
                    }
                    else
                    {
                        Cursor c = baseDatos.rawQuery("SELECT * FROM " + tablaProductos + " WHERE IdLista = " + id + ";", null);

                        try
                        {
                            if (c.moveToFirst())
                            {
                                do
                                {
                                    if (c.getString(1).equalsIgnoreCase(producto))
                                    {
                                        cancelar = true;
                                    }
                                } while (c.moveToNext());

                                if (!cancelar)
                                {
                                    actualizarProducto();

                                    Intent i = new Intent(EditItem.this, ListTable.class);
                                    i.putExtra("id", id);
                                    i.putExtra("nombre", nombrerecibido);
                                    startActivity(i);
                                }
                                else
                                {
                                    AlertDialog.Builder builder2 = new AlertDialog.Builder(EditItem.this);
                                    builder2.setTitle("Error");
                                    builder2.setMessage("Ya existe un item con ese nombre en la lista");
                                    builder2.setPositiveButton("OK",null);
                                    builder2.create();
                                    builder2.show();

                                    cancelar = false;
                                }
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "No existe tabla" + e);
                        }
                    }
                }
                else
                {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(EditItem.this);
                    builder2.setTitle("Error");
                    builder2.setMessage("Debe ingresar todos los campos");
                    builder2.setPositiveButton("OK",null);
                    builder2.create();
                    builder2.show();
                }
            }
        });

        Button eliminar = (Button) findViewById(R.id.buttonEliminarItem);
        eliminar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //nuevo
                new AlertDialog.Builder(EditItem.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Eliminar item")
                        .setMessage("¿Está seguro que desea eliminar este item?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                eliminarProducto();

                                Intent i = new Intent(EditItem.this, ListTable.class);
                                i.putExtra("id", id);
                                i.putExtra("nombre", nombrerecibido);
                                startActivity(i);
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed ()
    {
        Intent i = new Intent(EditItem.this, ListTable.class);
        baseDatos.close();
        i.putExtra("id", id);
        i.putExtra("nombre", nombrerecibido);
        startActivity(i);
    }

    private void abrirBasedatos()
    {
        try
        {
            baseDatos = openOrCreateDatabase(nombreBD, MODE_WORLD_WRITEABLE, null);
        }
        catch (Exception e)
        {
            Log.i(TAG, "Error al abrir o crear la base de datos" + e);
        }
    }

    private boolean actualizarProducto()
    {
        baseDatos.execSQL("UPDATE " + tablaProductos
                + " SET Producto = '" + producto + "',"
                + " Cantidad = '" + cantidad + "',"
                + " Precio = '" + precio
                + "' WHERE IdLista = '" + Integer.toString(id) + "' AND Producto = '" + nombreantiguo + "';");
        baseDatos.close();
        return true;
    }

    private boolean eliminarProducto()
    {
        baseDatos.execSQL("DELETE FROM " + tablaProductos + " WHERE IdLista = '" + Integer.toString(id) + "' AND Producto = '" + nombreantiguo + "';");
        baseDatos.close();
        return true;
    }
}
