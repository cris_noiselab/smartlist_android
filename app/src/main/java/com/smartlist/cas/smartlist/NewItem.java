package com.smartlist.cas.smartlist;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class NewItem extends AppCompatActivity
{
    protected int id;
    protected String nombrerecibido;

    protected String producto;
    protected int cantidad;
    protected float precio;
    protected boolean cancelar;

    protected EditText nombreitem;
    protected EditText cantidaditem;
    protected EditText precioitem;

    private SQLiteDatabase baseDatos;
    private static final String TAG = "bdlistapersonal";
    private static final String nombreBD = "listapersonal";

    private static final String tablaProductos = "Productos";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_item);


        cancelar = false;

        //Abrir la base de datos, se creara si no existe
        abrirBasedatos();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            id = Integer.parseInt(extras.get("id").toString());
            nombrerecibido = extras.get("nombre").toString();
        }

        nombreitem = (EditText) findViewById(R.id.editTextNombreItem);
        cantidaditem = (EditText) findViewById(R.id.editTextCantidadItem);
        precioitem = (EditText) findViewById(R.id.editTextPrecioItem);

        ImageButton regresar = (ImageButton) findViewById(R.id.ibuttonRetrocederListTable);
        regresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(NewItem.this, ListTable.class);
                baseDatos.close();
                i.putExtra("id", id);
                i.putExtra("nombre", nombrerecibido);
                startActivity(i);
            }
        });

        Button guardar = (Button) findViewById(R.id.buttonGuardar);
        guardar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!nombreitem.getText().toString().isEmpty() && !cantidaditem.getText().toString().isEmpty() && !precioitem.getText().toString().isEmpty())
                {
                    producto = nombreitem.getText().toString();
                    cantidad = Integer.parseInt(cantidaditem.getText().toString());
                    precio = Float.parseFloat(precioitem.getText().toString());

                    Cursor c = baseDatos.rawQuery("SELECT * FROM " + tablaProductos + " WHERE IdLista = " + id + ";", null);

                    try
                    {
                        if (c.moveToFirst())
                        {
                            do
                            {
                                if (c.getString(1).equalsIgnoreCase(producto))
                                {
                                    cancelar = true;
                                }
                            } while (c.moveToNext());

                            if (!cancelar)
                            {
                                insertarProducto();

                                Intent i = new Intent(NewItem.this, ListTable.class);
                                baseDatos.close();
                                i.putExtra("id", id);
                                i.putExtra("nombre", nombrerecibido);
                                startActivity(i);
                            }
                            else
                            {
                                AlertDialog.Builder builder2 = new AlertDialog.Builder(NewItem.this);
                                builder2.setTitle("Error");
                                builder2.setMessage("Ya existe un item con ese nombre en la lista");
                                builder2.setPositiveButton("OK",null);
                                builder2.create();
                                builder2.show();

                                cancelar = false;
                            }
                        }
                        else
                        {
                            insertarProducto();
                            Intent i = new Intent(NewItem.this, ListTable.class);
                            baseDatos.close();
                            i.putExtra("id", id);
                            i.putExtra("nombre", nombrerecibido);
                            startActivity(i);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.i(TAG, "No existe tabla" + e);
                    }
                }
                else
                {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(NewItem.this);
                    builder2.setTitle("Error");
                    builder2.setMessage("Debe ingresar todos los campos");
                    builder2.setPositiveButton("OK",null);
                    builder2.create();
                    builder2.show();
                }
            }
        });
    }

    @Override
    public void onBackPressed ()
    {
        Intent i = new Intent(NewItem.this, ListTable.class);
        i.putExtra("id", id);
        i.putExtra("nombre", nombrerecibido);
        baseDatos.close();
        startActivity(i);

    }


    private void abrirBasedatos()
    {
        try
        {
            baseDatos = openOrCreateDatabase(nombreBD, MODE_WORLD_WRITEABLE, null);
        }
        catch (Exception e)
        {
            Log.i(TAG, "Error al abrir o crear la base de datos" + e);
        }
    }

    private boolean insertarProducto()
    {
        ContentValues values = new ContentValues();
        values.put("IdLista", id);
        values.put("Producto", producto);
        values.put("Cantidad", cantidad);
        values.put("Precio", precio);

        return (baseDatos.insert(tablaProductos, null, values) > 0);
    }
}