package com.smartlist.cas.smartlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Carlos on 04/08/2017.
 */

public class AdapterDaproductos extends BaseAdapter
{
    protected Activity activity;
    protected ArrayList<Daproductos> items;

    public AdapterDaproductos (Activity activit, ArrayList<Daproductos> item)
    {
        activity = activit;
        items = item;
    }

    @Override
    public int getCount()
    {
        return items.size();
    }

    @Override
    public Object getItem(int position)
    {
        return items.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        if (convertView == null)
        {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.da_productos, null);
        }

        Daproductos da = items.get(position);
        TextView producto = (TextView) v.findViewById(R.id.textViewProductoDa);
        producto.setText(da.getProducto());
        TextView precio = (TextView) v.findViewById(R.id.textViewPrecioDa);
        precio.setText(da.getPrecio());
        TextView cantidad = (TextView) v.findViewById(R.id.textViewCantidadDa);
        cantidad.setText(da.getCantidad());
        TextView total = (TextView) v.findViewById(R.id.textViewTotalDa);
        total.setText(da.getTotal());

        return v;
    }
}
