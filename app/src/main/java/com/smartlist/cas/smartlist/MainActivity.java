package com.smartlist.cas.smartlist;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.startAnimation();


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    public void startAnimation(){
        ImageView img= (ImageView) findViewById(R.id.imageView);
        AnimationDrawable animation = new AnimationDrawable();
        animation.addFrame(getResources().getDrawable(R.drawable.bitroomicon1), 1000);
        animation.addFrame(getResources().getDrawable(R.drawable.bitroomicon2), 1000);
        animation.addFrame(getResources().getDrawable(R.drawable.bitroomicon1), 1200);
        animation.addFrame(getResources().getDrawable(R.drawable.bitroomicon2), 800);
        animation.addFrame(getResources().getDrawable(R.drawable.bitroomicon1), 800);
        animation.setOneShot(true);
        img.setImageDrawable(animation);

        // start the animation!
        animation.start();


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                goMainMenu();
            }
        }, 4900);
    }
    public void goMainMenu(){
        Intent i = new Intent(MainActivity.this, MainMenu.class);
        startActivity(i);
    }



}