package com.smartlist.cas.smartlist;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListTable extends AppCompatActivity
{
    protected int i;

    protected int id;
    protected String nombrerecibido;

    protected String[] productos;
    protected int[] cantidades;
    protected float[] precios;
    protected float[] totales;

    protected float total;

    protected ListView producto;

    protected AdapterDaproductos adaptador;
    protected Daproductos item;

    private SQLiteDatabase baseDatos;
    private static final String TAG = "bdlistapersonal";
    private static final String nombreBD = "listapersonal";

    private static final String tablaListas = "Listas";
    private static final String tablaProductos = "Productos";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_table);


        id = 0;
        i = 0;

        //Abrir la base de datos, se creara si no existe
        abrirBasedatos();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            id = Integer.parseInt(extras.get("id").toString());
            nombrerecibido = extras.get("nombre").toString();
        }

        producto = (ListView) findViewById(R.id.listViewItems);

        llenarProductos();

        TextView nombreLista = (TextView) findViewById(R.id.textViewNombreLista);
        nombreLista.setText(nombrerecibido);

        ImageButton regresar = (ImageButton) findViewById(R.id.ibuttonRetrocederLists);
        regresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(ListTable.this, Lists.class);
                baseDatos.close();
                startActivity(i);
            }
        });

        final ImageButton eliminar = (ImageButton) findViewById(R.id.ibuttonEliminarLista);
        eliminar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //nuevo
                new AlertDialog.Builder(ListTable.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Eliminar lista")
                        .setMessage("¿Está seguro que desea eliminar esta lista?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                eliminarProductosLista();
                                eliminarLista();
                                Intent i = new Intent(ListTable.this, Lists.class);
                                baseDatos.close();
                                startActivity(i);
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        ImageButton agregarItem = (ImageButton) findViewById(R.id.ibuttonAgregarItem);
        agregarItem.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(ListTable.this, NewItem.class);
                baseDatos.close();
                i.putExtra("id", id);
                i.putExtra("nombre", nombrerecibido);
                startActivity(i);
            }
        });

        producto.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id1)
            {
                Intent i = new Intent(ListTable.this, EditItem.class);
                baseDatos.close();
                i.putExtra("id", id);
                i.putExtra("nombre", nombrerecibido);
                i.putExtra("nombreantiguo", productos[position]);
                i.putExtra("cantidadantiguo", cantidades[position]);
                i.putExtra("precioantiguo", precios[position]);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed ()
    {
        Intent i = new Intent(ListTable.this, Lists.class);
        baseDatos.close();
        startActivity(i);
    }

    private void abrirBasedatos()
    {
        try
        {
            baseDatos = openOrCreateDatabase(nombreBD, MODE_WORLD_WRITEABLE, null);
        }
        catch (Exception e)
        {
            Log.i(TAG, "Error al abrir o crear la base de datos" + e);
        }
    }

    private boolean eliminarProductosLista()
    {
        String[] whereArgs = new String[] { Integer.toString(id) };
        return (baseDatos.delete(tablaProductos, "IdLista = ?", whereArgs) > 0);
    }

    private boolean eliminarLista()
    {
        String[] whereArgs = new String[] { Integer.toString(id) };
        return (baseDatos.delete(tablaListas, "Id = ?", whereArgs) > 0);
    }

    private void llenarProductos()
    {
        total = 0;

        Cursor c = baseDatos.rawQuery("SELECT * FROM " + tablaProductos + " WHERE IdLista = '" + id + "';", null);
        ArrayList<Daproductos> daproductos = new ArrayList<Daproductos>();

        TextView texttotal = (TextView) findViewById(R.id.textViewTotal);

        try
        {
            if (c.moveToFirst())
            {
                productos = new String[c.getCount()];
                cantidades = new int[c.getCount()];
                precios = new float[c.getCount()];
                totales = new float[c.getCount()];

                do
                {
                    productos[i] = c.getString(1);
                    cantidades[i] = c.getInt(2);
                    precios[i] = c.getFloat(3);
                    totales[i] = (cantidades[i] * precios[i]);
                    total += totales[i];
                    item = new Daproductos(productos[i], String.valueOf(precios[i]), "X " + cantidades[i], String.valueOf(totales[i]));
                    daproductos.add(item);
                    i++;
                }while(c.moveToNext());

                String stotal = String.format("%.2f", total);
                texttotal.setText("Total $ " + stotal);

                i = 0;

                adaptador = new AdapterDaproductos(ListTable.this, daproductos);
                producto.setAdapter(adaptador);
            }
        }
        catch (Exception e)
        {
            Log.i(TAG, "No existe tabla" + e);
        }
    }
}