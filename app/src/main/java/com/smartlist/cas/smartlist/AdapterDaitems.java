package com.smartlist.cas.smartlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Carlos on 03/08/2017.
 */

public class AdapterDaitems extends BaseAdapter
{
    protected Activity activity;
    protected ArrayList<Daitems> items;

    public AdapterDaitems (Activity activit, ArrayList<Daitems> item)
    {
        activity = activit;
        items = item;
    }

    @Override
    public int getCount()
    {
        return items.size();
    }

    @Override
    public Object getItem(int position)
    {
        return items.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        if (convertView == null)
        {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.da_items, null);
        }

        Daitems da = items.get(position);
        TextView lista = (TextView) v.findViewById(R.id.textViewLista);
        lista.setText(da.getLista());
        TextView fecha = (TextView) v.findViewById(R.id.textViewFecha);
        fecha.setText(da.getFecha());

        return v;
    }
}
