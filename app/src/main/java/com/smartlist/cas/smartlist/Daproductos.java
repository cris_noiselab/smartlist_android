package com.smartlist.cas.smartlist;

/**
 * Created by Carlos on 04/08/2017.
 */

public class Daproductos
{
    protected String producto;
    protected String precio;
    protected String cantidad;
    protected String total;
    protected int id;

    public Daproductos (String product, String preci, String cantida, String tota)
    {
        producto = product;
        precio = preci;
        cantidad = cantida;
        total = tota;
    }

    public String getProducto ()
    {
        return producto;
    }

    public void setProducto (String product)
    {
        producto = product;
    }

    public String getPrecio ()
    {
        return precio;
    }

    public void setPrecio (String preci)
    {
        precio = preci;
    }

    public String getCantidad ()
    {
        return cantidad;
    }

    public void setCantidad (String cantida)
    {
        cantidad = cantida;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String tota)
    {
        total = tota;
    }

    public int getId ()
    {
        return id;
    }

    public void setId (int i)
    {
        id = i;
    }
}