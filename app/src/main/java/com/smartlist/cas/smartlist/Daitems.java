package com.smartlist.cas.smartlist;

/**
 * Created by Carlos on 03/08/2017.
 */

public class Daitems
{
    protected String lista;
    protected String fecha;
    protected int id;

    public Daitems (String list, String fech)
    {
        lista = list;
        fecha = fech;
    }

    public String getLista ()
    {
        return lista;
    }

    public void setLista (String list)
    {
        lista = list;
    }

    public String getFecha ()
    {
        return fecha;
    }

    public void setFecha (String fech)
    {
        fecha = fech;
    }

    public int getId ()
    {
        return id;
    }

    public void setId (int i)
    {
        id = i;
    }
}